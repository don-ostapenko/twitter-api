@foreach($users as $user)
<div class="card mb-2">
    <div class="card-body">
        <div class="d-flex flex-row align-items-center mb-3">
            <img src="{{ $user->profile_image_url }}" alt="avatar" width="35" height="35" class="rounded-circle">
            <div class="pl-3">
                <h5 class="card-title m-0">{{ $user->name }}</h5>
                <small id="emailHelp" class="form-text text-muted m-0">@ {{ $user->screen_name }}</small>
            </div>
        </div>

        <a href="{{ route('home.show', $user) }}" class="btn btn-outline-primary btn-sm mb-2"  role="button">Show tweets</a>
        <form action="{{ route('home.destroy', $user) }}" method="post">
            @method('delete')
            @csrf
            <button type="submit" class="btn btn-outline-danger btn-sm">Remove user</button>
        </form>
    </div>
</div>
@endforeach
