@foreach($tweets as $tweet)
    <div class="card mb-4">
        <div class="card-body">
            <h5 class="card-title mb-2">{{ $tweet['title'] }}</h5>
            <small id="emailHelp" class="form-text text-muted m-0">{{ date('F j, G:i', $tweet['created_at']) }}</small>
            <p class="card-text mt-2">{{ $tweet['text'] }}</p>
        </div>
    </div>
@endforeach
