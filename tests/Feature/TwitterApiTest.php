<?php

namespace Tests\Feature;

use App\Http\Middleware\TwitterApiAuthentication;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class TwitterApiTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
    }

    public function testAuthentication()
    {
        $response = $this->get('/api/v1/twitters');
        $response->assertStatus(401);
    }

    public function testThatTweetsFounded()
    {
        $response = $this->get('/api/v1/twitters?screen_name=elonmusk',
            self::getKey());
        $response->assertStatus(200);
    }

    public function testThatTweetsNotFound()
    {
        $response = $this->get('/api/v1/twitters?screen_name=unknow',
            self::getKey());
        $response->assertStatus(404);
    }

    public function testThatParameterNotPassed()
    {
        $response = $this->get('/api/v1/twitters?screen_name=',
            self::getKey());
        $response->assertStatus(422);
    }

    public function testThatUserAdded()
    {
        $response = $this->post('/api/v1/twitters?screen_name=Schwarzenegger',
            [],
            self::getKey());
        $response->assertStatus(201);
    }

    public function testThatUserExist()
    {
        $this->post('/api/v1/twitters?screen_name=Schwarzenegger',
            [],
            self::getKey());
        $response = $this->post('/api/v1/twitters?screen_name=Schwarzenegger',
            [],
            self::getKey());
        $response->assertStatus(422);
    }

    public function testThatUserDeleted()
    {
        $this->post('/api/v1/twitters?screen_name=Schwarzenegger',
            [],
            self::getKey());
        $response = $this->delete('/api/v1/twitters/Schwarzenegger',
            [],
            self::getKey());
        $response->assertStatus(204);
    }

    public function testThatUserDoesNotExistDuringDelete()
    {
        $response = $this->delete('/api/v1/twitters/Schwarzenegger',
            [],
            self::getKey());
        $response->assertStatus(404);
    }

    private static function getKey()
    {
        return [TwitterApiAuthentication::API_KEY => config('services.api.token')];
    }

}
