<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TwitterUsersTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Elon Musk',
                'screen_name' => 'elonmusk',
                'profile_image_url' => 'http://pbs.twimg.com/profile_images/1223183340171415552/XQcxk5Zb_normal.jpg',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time())
            ],
            [
                'name' => 'Neil deGrasse Tyson',
                'screen_name' => 'neiltyson',
                'profile_image_url' => 'http://pbs.twimg.com/profile_images/74188698/NeilTysonOriginsA-Crop_normal.jpg',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time())
            ],
            [
                'name' => 'Pete Souza',
                'screen_name' => 'PeteSouza',
                'profile_image_url' => 'http://pbs.twimg.com/profile_images/821790760618127360/wPF7B89y_normal.jpg',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time())
            ],
            [
                'name' => 'Taylor Swift',
                'screen_name' => 'taylorswift13',
                'profile_image_url' => 'http://pbs.twimg.com/profile_images/1201195539888590848/eSnkZy2V_normal.jpg',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time())
            ],
        ];

        DB::table('twitter_users')->insert($data);
    }

}
