## About App

This app contains REST API for interaction with Twitter API. API tests are implemented. Their statistic is available by the "tests/Coverage" way in HTML format.

Steps of deployment

- Clone the project.
- Run `docker-compose up -d` in root project.
- Run `docker-compose exec php bash`.
- Run `composer install` in a container.
- Run `php artisan migrate:fresh --seed` in a container.

## REST API

Use Postman for REST API testing.


## (POST request) Add Twitter user
- For adding a user via API method use resource URL with a Twitter screen name. Parameter "screen_name" is required.

Resource URL
```
http://twitter.localhost/api/v1/twitters
```

Example Request
```
$ curl --request POST 
    --url 'http://twitter.localhost/api/v1/twitters?screen_name=TwitterDev' 
    --header 'x-api-key: <token>'
```

## (GET request) Get last tweets of Twitter user
- For getting last tweets via API method use resource URL with a Twitter screen name and number of tweets. Parameter "screen_name" is required, the "count" is optional.

Resource URL
```
http://twitter.localhost/api/v1/twitters
```

Example Request
```
$ curl --request GET 
    --url 'http://twitter.localhost/api/v1/twitters?screen_name=TwitterDev&count=2' 
    --header 'x-api-key: <token>'
```

Example Response
```
[
    {
        "text": "As we work to keep our employees safe during COVID-19, you are likely to experience longer than usual review times… https://t.co/Oo1t07UH4Z",
        "created_at": 1585000873,
        "title": "Tweet: 1 by Twitter Dev"
    },
    {
        "text": "The Tweets and recent search endpoints will now also include annotations. \n\nRead more 📖 https://t.co/xpjZgICl5b",
        "created_at": 1584980769,
        "title": "Tweet: 2 by Twitter Dev"
    }
]
```

## (DELETE request) Delete Twitter user
- For deleting a user via API method use resource URL with a slug "twitter".

Resource URL
```
http://twitter.localhost/api/v1/twitters/{twitters}
```

Example Request
```
$ curl --request GET 
    --url 'http://twitter.localhost/api/v1/twitters/TwitterDev' 
    --header 'x-api-key: <token>'
```
