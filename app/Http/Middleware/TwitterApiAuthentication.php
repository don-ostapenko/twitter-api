<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class TwitterApiAuthentication
{

    const API_KEY = 'x-api-key';

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $token = $request->header(self::API_KEY);
        $error = ['error' => [
            'error' => 'Unauthorized.',
            'status' => 401,
        ]];

        if ($token === null) {
            return response()->json($error, 401);
        }

        if ($token !== config('services.api.token')) {
            return response()->json($error, 401);
        }

        return $next($request);
    }

}
