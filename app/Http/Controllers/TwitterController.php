<?php

namespace App\Http\Controllers;

use App\Http\Requests\TwitterRequest;
use App\Models\TwitterUser;
use App\Services\TwitterApiServiceInterface;

class TwitterController extends Controller
{

    /**
     * @var \App\Services\TwitterApiServiceInterface
     */
    protected $twitterApiService;

    /**
     * TwitterController constructor.
     *
     * @param  \App\Services\TwitterApiServiceInterface $twitterApiService
     */
    public function __construct(TwitterApiServiceInterface $twitterApiService)
    {
        $this->twitterApiService = $twitterApiService;
    }

    /**
     * Store a user in storage.
     *
     * @param  \App\Http\Requests\TwitterRequest  $request
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function store(TwitterRequest $request)
    {
        $screenName = $request->get('screen_name');
        $user = TwitterUser::where('screen_name', $screenName)->first();

        if ($user) {
            return response()->json('User already exist', '422');
        }

        $this->twitterApiService->storeUserByScreenName($screenName);

        return response()->json('User was successfully created', '201');
    }

    /**
     * Display needed count of tweets.
     *
     * @param  \App\Http\Requests\TwitterRequest  $request
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function show(TwitterRequest $request)
    {
        $screenName = $request->get('screen_name');
        $count = $request->get('count');

        $response = $this->twitterApiService
            ->getTweetsByScreenName($screenName,
            $count);

        if (empty($response)) {
            return response()->json('User not found', '404');
        }

        return response()->json($response, '200');
    }

    /**
     * Remove a user from storage.
     *
     * @param  \App\Models\TwitterUser  $twitter
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(TwitterUser $twitter)
    {
        if (empty($twitter)) {
            return response()->json('User not found.', '404');
        }

        $twitter->delete();
        return response()->json('', '204');
    }

}
