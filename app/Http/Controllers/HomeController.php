<?php

namespace App\Http\Controllers;

use App\Models\TwitterUser;
use App\Services\TwitterApiServiceInterface;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    /**
     * @var \App\Services\TwitterApiServiceInterface
     */
    protected $twitterApiService;

    /**
     * TwitterController constructor.
     *
     * @param  \App\Services\TwitterApiServiceInterface $twitterApiService
     */
    public function __construct(TwitterApiServiceInterface $twitterApiService)
    {
        $this->twitterApiService = $twitterApiService;
    }

    /**
     * Main page app with list of users.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $users = TwitterUser::all();
        $tweets = [];
        return view('home.index', compact('tweets','users'));
    }

    /**
     * Store a chosen user to DB.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function store(Request $request)
    {
        $screenName = $request->post('screen_name');
        $user = TwitterUser::where('screen_name', $screenName)->first();

        if (!$user) {
            $this->twitterApiService->storeUserByScreenName($screenName);
            return redirect()->route('home.index')
                ->with('success', 'The user was successfully created.');
        }

        return redirect()->route('home.index')
            ->with('error', sprintf('The user %s already exist.', $user->screen_name));
    }

    /**
     * Show latest 10 tweets by chosen user.
     *
     * @param  \App\Models\TwitterUser  $twitterUser
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function show(TwitterUser $twitterUser)
    {
        $users = TwitterUser::all();
        $tweets = $this->twitterApiService->getTweetsByScreenName($twitterUser->screen_name);
        return view('home.index', compact('users', 'tweets'));
    }

    /**
     * Remove a twitter-user from DB.
     *
     * @param  \App\Models\TwitterUser  $twitterUser
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(TwitterUser $twitterUser)
    {
        $twitterUser->delete();
        return redirect()->route('home.index')->with('success', 'The user was successfully removed.');
    }

}
