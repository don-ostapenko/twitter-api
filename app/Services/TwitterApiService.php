<?php

namespace App\Services;

use Abraham\TwitterOAuth\TwitterOAuth;
use App\Models\TwitterUser;

class TwitterApiService implements TwitterApiServiceInterface
{

    /**
     * @var \Abraham\TwitterOAuth\TwitterOAuth
     */
    protected $connection;

    /**
     * TwitterApiService constructor.
     */
    public function __construct()
    {
        $this->connection = new TwitterOAuth(
            config('twitter_api.consumer_key'),
            config('twitter_api.consumer_secret'));
    }

    /**
     * {@inheritDoc}
     */
    public function storeUserByScreenName(string $screenName = null): void
    {
        if (empty($screenName)) {
            throw new \Exception('Screen name is empty.');
        }

        $response = $this->connection->get('users/show',
            ['screen_name' => $screenName]
        );

        if ($this->connection->getLastHttpCode() != 200) {
            throw new \Exception(sprintf('%s Code: %d.',
                $response->errors[0]->message,
                $response->errors[0]->code
            ));
        }

        TwitterUser::create([
            'name' => $response->name,
            'screen_name' => $response->screen_name,
            'profile_image_url' => $response->profile_image_url,
        ]);
    }

    /**
     * Method that retrieve 10 tweets by screen name parameter.
     *
     * @param  string  $screenName
     * @param  int  $count
     *
     * @return array
     * @throws \Exception
     */
    public function getTweetsByScreenName(string $screenName, int $count = null): array
    {

        $response = $this->connection->get('statuses/user_timeline',
            ['screen_name' => $screenName, 'count' => $count]
        );

        if ($this->connection->getLastHttpCode() != 200) {
            throw new \Exception(sprintf(
                '%s Code: %d.',
                $response->errors[0]->message,
                $response->errors[0]->code
            ));
        }

        return $this->parseTweetsToBaseView($response);
    }

    /**
     * Method that parse data to base view.
     *
     * @param  array  $tweets
     *
     * @return array
     */
    protected function parseTweetsToBaseView(array $tweets): array
    {
        $parsedTweets = [];

        for ($i = 0; $i < count($tweets); $i++) {
            $parsedTweets[$i]['text'] = $tweets[$i]->text;
            $parsedTweets[$i]['created_at'] = strtotime($tweets[$i]->created_at);
            $parsedTweets[$i]['title'] = 'Tweet: ' . ($i + 1) . ' by ' . $tweets[$i]->user->name;
        }

        return $parsedTweets;
    }

}
