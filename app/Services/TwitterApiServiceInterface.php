<?php

namespace App\Services;

interface TwitterApiServiceInterface
{

    /**
     * Method that store a user by screen-name parameter in DB.
     *
     * @param  string|null  $screenName
     *
     * @throws \Exception
     */
    public function storeUserByScreenName(string $screenName = null): void;

}
