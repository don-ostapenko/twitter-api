<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TwitterUser extends Model
{

    /**
     * @var array
     */
    protected $fillable = ['name', 'screen_name', 'profile_image_url'];

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @return string
     */
    public function getRouteKeyName(): string
    {
        return 'screen_name';
    }

}
